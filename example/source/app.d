import vibe.vibe;

void main()
{
    auto router = new URLRouter;
    router.registerWebInterface(new WebInterface);

    auto settings = new HTTPServerSettings;
    settings.port = 8080;
    settings.bindAddresses = ["::1", "127.0.0.1"];
    listenHTTP(settings, router);

    logInfo("Please open http://127.0.0.1:8080/ in your browser.");
    runApplication();
}

class WebInterface {
    struct Context {
        string doc_lang;
        string doc_body_attrs;
    }
    Context context;

    this() {
        context.doc_lang = "de";
        context.doc_body_attrs = "dir=\"ltr\"";
    }

    void index() {
        render!("index.dt", context);
        // The context is optional.
        // render!("index.dt");
    }
}
