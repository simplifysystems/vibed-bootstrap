# Vibe.d Bootstrap Component

This provides a quick Bootstrap 4.0.0-alpha6 template for projects, using the
Bootstrap CDN.

Add bootstrap to your project via `extends bootstrap_layout` in your Diet template.

## Layout Blocks

The following blocks are provided (in a Diet file); note that you will want to
append to most blocks rather than replace them unless you intentionally wish to
replace a script or stylesheet. Replacing the `head` block in your template
would replace all child blocks (meta, styles, scripts, title), etc.

```pug
//- In the <head>
block head
    block meta
    block styles
    block scripts
    block title
//- In the <body>
block body
    block navbar
    block content
block body_scripts
```

# Document Modification

You can inject a couple of values into the template; you can currently modify
the `lang` attribute of the \<html\> tag and add attributes to the \<body\> tag.
Defaults are used if nothing is specified.

You may pass a `context` to the render function; the language and body properties are currently the only customizations you can make.

```D
class WebInterface {
    struct Context {
        string doc_lang;
        string doc_body_tag;
    }
    // The context variable is required, but can be empty; any fields not
    // declared will have defaults assigned.
    Context context;

    this() {
        // Will create: <html lang="de">
        context.doc_lang = "de";
        // Will create: <body dir="ltr">;
        context.doc_body_attr = "dir=\"ltr\"";
    }

    void index() {
        render!("index.dt", context);
        // The context is optional.
        // render!("index.dt");
    }
}
```
